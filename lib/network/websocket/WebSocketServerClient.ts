'use strict';

import * as WebSocket from 'ws';

import { WebSocketClient } from './WebSocketClient';

export class WebSocketServerClient extends WebSocketClient {
	public remoteAddress: string;
	public remotePort: number;
	public remoteFamily: 'IPV4' | 'IPV6';

	public lastHeartbeat: number;

	constructor(socket: WebSocket, id: string) {
		super();

		this.id = id;
		this.attach(socket);
	}
}
