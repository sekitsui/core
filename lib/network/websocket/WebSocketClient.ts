
import { EventEmitter } from 'events';

import * as WebSocket from 'ws';

import {
	SekitsuiCloseCode,
	SekitsuiOPCode,
} from '../../constants';

import {
	SekitsuiHeartbeat,
	SekitsuiMessage,
} from '../../interfaces';

export interface SendMessageOptions {
	// request delivery comfirmation
	confirm?: boolean;

	// auto retry on delivery failure
	retry?: boolean;

	// max ammount of times to retry delivery
	maxRetries?: number;
}

/**
 * An agnostic representation of a SocketClient
 * Used as the base for both "sides" of the connection
 * Clients and ServerClients
 */
export class WebSocketClient extends EventEmitter {
	public id: string;
	public socket: WebSocket;

	private properties: Map<string, any> = new Map();

	/**
	 * Attaches a WebSocket instance
	 * @param socket - WebSocket instance
	 */
	public attach(socket: WebSocket) {
		if (this.socket != null) this.socket.removeAllListeners();
		this.socket = socket;

		// attach event listeners
		// TODO: keep track of rebinded handler functions so detach doesn't have to call removeAllListeners
		this.socket.on('open', this.handleOpen.bind(this));
		this.socket.on('close', this.handleClose.bind(this));
		this.socket.on('error', this.handleError.bind(this));

		this.socket.on('message', this.handleMessageProxy.bind(this));
	}

	/**
	 * Detaches any currently connected WebSocket and clears all listeners
	 * @returns {boolean} success
	 */
	public detach() {
		if (this.socket == null) return true;

		this.socket.removeAllListeners();
		this.socket = null;

		return true;
	}

	/**
	 * Verify message syntax and socket state, then send message
	 * @param message Object or string to send
	 * @param options Delivery options
	 * @returns {Promise} Resolves if delievery was successful
	 */
	public async sendMessage(message: SekitsuiMessage | string, options?: SendMessageOptions) {
		if (this.socket == null) throw new Error('State Error: No WebSocket has been attached');

		if (typeof message === 'object') {
			if (!message.op && message.op !== 0) throw new Error('Malformed Message: Must have valid OPCode.');
			if (!message.d && message.d !== null) throw new Error('Malformed Message: message data must be defined.');

			message = JSON.stringify(message);
		}

		options = Object.assign({}, {
			confirm: false,
			retry: true,
			maxRetries: 3,
		}, options);

		if (this.socket.readyState !== WebSocket.OPEN) {
			throw new Error('WebSocket readyState is not open.');
		}

		return new Promise<void>((resolve, reject) => {
			let attempt = 0;
			(function send() {
				this.socket.send(message, (e: Error) => {
					if (e) {
						if (options.retry && attempt++ < options.maxRetries) send.call(this);
						else reject(e);
						return;
					}
					resolve();
				});
			}).call(this);
		});
	}

	/**
	 * Close the underlying WebSocket Connection
	 * @param code Close Code
	 * @param reason Human readable reason for the closure
	 */
	public close(code: number, reason: string) {
		if (this.socket == null) throw new Error('State Error: No WebSocket has been attached');

		this.socket.close(code, reason);
	}

	/**
	 * Called when connection is opened, this is won't be called on ServerClients
	 * @override
	 */
	public async handleOpen() {
		this.emit('open');
	}

	/**
	 * Called when underlying WebSocket close event occurs
	 * @override
	 * @param code Close code
	 * @param reason Human readable reason
	 */
	public async handleClose(code: number, reason: string) {
		this.emit('close', code, reason);
	}

	/**
	 * Called when underlying WebSocket error event occurs
	 * @override
	 * @param e Error
	 */
	public async handleError(e: Error) {
		this.emit('error', e);
	}

	/**
	 * Converts raw message string into native Object
	 * Additionally verifies valid message syntax, if malformed
	 * will auto kill connection.
	 * @param raw Raw WebSocket message data
	 * @returns {SekitsuiMessage} Parsed SekitsuiMessage
	 */
	public async parseMessage(raw: string): Promise<SekitsuiMessage> {
		let message: SekitsuiMessage = null;
		try {
			message = JSON.parse(raw);
		} catch (e) {
			throw new Error('Malformed Message: Failed to decode message');
		}

		// verify message
		if (typeof message !== 'object') throw new Error('Malformed Message: Message must be an Object');
		if (typeof message.op !== 'number') throw new Error('Malformed Message: No opcode');
		if (typeof message.d === 'undefined') throw new Error('Malformed Message: Data undefined');

		return message;
	}

	/**
	 * "Side" specific handling of messages
	 * @override
	 * @param message SekitsuiMessage
	 */
	public async handleMessage(message: SekitsuiMessage) {
		return;
	}

	/**
	 * Handles "side" agnostic messages. And calls handleMessage
	 * @param data Raw WebSocket Message
	 */
	private async handleMessageProxy(data: WebSocket.Data) {
		let message: SekitsuiMessage = null;
		try {
			message = await this.parseMessage(data.toString());
		} catch (e) {
			this.close(SekitsuiCloseCode.DECODE_ERROR, e.message || 'Failedto parse message');
			return;
		}

		this.emit('message', message);

		switch (message.op) {
			case SekitsuiOPCode.DISPATCH: {
				if (typeof message.t !== 'string') {
					this.close(SekitsuiCloseCode.INVALID_DISPATCH, 'Malformed Dispatch: No type');
					return;
				}

				this.emit('dispatch', message);
				break;
			}

			case SekitsuiOPCode.HEARTBEAT: {
				await this.sendMessage({
					op: SekitsuiOPCode.HEARTBEAT_ACK,
					d: null,
				} as SekitsuiHeartbeat);
				break;
			}

			case SekitsuiOPCode.READY: {
				this.emit('ready');
				break;
			}

			default: // NO-OP
		}

		if (this.handleMessage) await this.handleMessage(message);
	}
}
