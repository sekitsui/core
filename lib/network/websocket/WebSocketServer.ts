
import { EventEmitter } from 'events';
import { IncomingMessage } from 'http';

import * as WebSocket from 'ws';

import { timedListener } from '@ayana/emitter-tools';

import { createID } from '../../util';

import { SekitsuiCloseCode, SekitsuiOPCode } from '../../constants';
import {
	SekitsuiHello,
	SekitsuiIdentify,
	SekitsuiMessage,
	SekitsuiReady,
} from '../../interfaces';

import { WebSocketServerClient } from './WebSocketServerClient';

export interface WebSocketServerProperties {
	version: string;
	[key: string]: any;
}

export interface WebSocketServerOptions {
	port: number;
	host?: string;

	heartbeatInterval?: number;
	behindProxy?: boolean;
	validateIdentify?(identify?: SekitsuiIdentify): { success: boolean, error?: string };

	properties: WebSocketServerProperties;
	[key: string]: any;
}

export enum WebSocketServerEvent {
	READY = 'ready',
	CLIENT_CREATE = 'clientCreate',
	CLIENT_DELETE = 'clientDelete',
	CLIENT_READY = 'clientReady',
	CLIENT_MESSAGE = 'clientMessage',
	CLIENT_CLOSE = 'clientClose',
	CLIENT_ERROR = 'clientError',
}

export class WebSocketServer extends EventEmitter {
	public id: string;

	public host: string;
	public port: number;

	public options: WebSocketServerOptions;

	private socket: WebSocket.Server;

	private clients: Map<string, WebSocketServerClient> = new Map();
	private pendingClients: Map<string, WebSocketServerClient> = new Map();

	constructor(options: WebSocketServerOptions) {
		super();

		if (options == null || typeof options !== 'object') throw new Error(`WebSocketServerOptions must be an object`);
		if (typeof options.port !== 'number') throw new Error(`Port must be a number`);

		if (options.properties == null || typeof options.properties !== 'object') throw new Error(`WebSocketServerProperties must be an object`);
		if (options.properties.version == null || typeof options.properties.version !== 'string') throw new Error(`Version must be a valid string`);

		this.options = Object.assign({}, {
			host: '127.0.0.1',
			heartbeatInterval: 30 * 1000,
			behindProxy: false,
			validateIdentify: () => ({ success: true }),
		} as WebSocketServerOptions, options);

		// start heartbeat monitor
		this.checkClientHeartbeats();
	}

	public async init() {
		this.id = createID(24);
		this.socket = new WebSocket.Server({
			port: this.options.port,
			host: this.options.host,
		});

		// set alias
		this.port = this.options.port;
		this.host = this.options.host;

		this.socket.on('listening', () => {
			this.emit(WebSocketServerEvent.READY, this.options.port, this.options.host);
		});

		this.socket.on('connection', this.onConnection.bind(this));
	}

	private async onConnection(socket: WebSocket, req: IncomingMessage) {
		let remoteAddress = req.connection.remoteAddress;
		// handle being behind a proxy
		if (this.options.behindProxy && req.headers['x-forwarded-for']) {
			remoteAddress = req.headers['x-forwarded-for'].toString();
		}

		// use left-most address (that's the client)
		remoteAddress = remoteAddress.split(',')[0].trim();
		const remotePort = req.connection.remotePort;
		const remoteFamily = req.connection.remoteFamily;

		const id = createID();
		const webSocketServerClient = new WebSocketServerClient(socket, id);

		// set remote connection information
		webSocketServerClient.remoteAddress = remoteAddress;
		webSocketServerClient.remotePort = remotePort;
		webSocketServerClient.remoteFamily = remoteFamily === 'IPV6' ? 'IPV6' : 'IPV4';

		// handle close and error
		webSocketServerClient.on('close', (code: number, data?: string) => {
			if (this.pendingClients.has(webSocketServerClient.id)) this.pendingClients.delete(webSocketServerClient.id);
			if (this.clients.has(webSocketServerClient.id)) this.clients.delete(webSocketServerClient.id);

			// stop listening for events
			webSocketServerClient.removeAllListeners();

			this.emit(WebSocketServerEvent.CLIENT_CLOSE, webSocketServerClient, code, data);
			this.emit(WebSocketServerEvent.CLIENT_DELETE, webSocketServerClient);
		});

		webSocketServerClient.on('error', (e: Error) => {
			if (this.pendingClients.has(webSocketServerClient.id)) this.pendingClients.delete(webSocketServerClient.id);
			if (this.clients.has(webSocketServerClient.id)) this.clients.delete(webSocketServerClient.id);
			webSocketServerClient.removeAllListeners();

			this.emit(WebSocketServerEvent.CLIENT_ERROR, webSocketServerClient, e);
			this.emit(WebSocketServerEvent.CLIENT_DELETE, webSocketServerClient);
		});

		// drop into pending
		this.pendingClients.set(webSocketServerClient.id, webSocketServerClient);
		this.emit(WebSocketServerEvent.CLIENT_CREATE, webSocketServerClient);

		const handleMessage = (identify: SekitsuiMessage): boolean => {
			if (identify.op !== SekitsuiOPCode.IDENTIFY) {
				// heartbeats are allowed
				if (identify.op === SekitsuiOPCode.HEARTBEAT || identify.op === SekitsuiOPCode.HEARTBEAT_ACK) return;

				// everything else is not
				webSocketServerClient.close(SekitsuiCloseCode.NOT_IDENTIFIED, 'You sent a payload prior to identifying. Don\'t do this!');

				return;
			}

			const { success, error } = this.options.validateIdentify(identify as SekitsuiIdentify);
			if (success === false) {
				webSocketServerClient.close(SekitsuiCloseCode.INVALID_IDENTIFY, error || 'Failed to identify');
				return false;
			}

			// client is now considered identified

			// refresh heartbeat
			webSocketServerClient.lastHeartbeat = Date.now();

			if (this.pendingClients.has(webSocketServerClient.id)) this.pendingClients.delete(webSocketServerClient.id);
			this.clients.set(webSocketServerClient.id, webSocketServerClient);

			this.emit(WebSocketServerEvent.CLIENT_READY, webSocketServerClient);

			// handle messages
			webSocketServerClient.on('message', async (message: SekitsuiMessage) => {
				// handle heartbeats
				if (message.op === SekitsuiOPCode.HEARTBEAT) {
					webSocketServerClient.lastHeartbeat = Date.now();

					try {
						webSocketServerClient.sendMessage({
							op: SekitsuiOPCode.HEARTBEAT_ACK,
							d: null,
						});
					} catch (e) {
						this.emit(WebSocketServerEvent.CLIENT_ERROR, webSocketServerClient, new Error(`Failed to send HEARTBEAT_ACK`));
					}
				}

				this.emit(WebSocketServerEvent.CLIENT_MESSAGE, webSocketServerClient, message);
			});

			return true;
		};

		timedListener(webSocketServerClient, [
			{ eventName: 'message', fn: handleMessage },
		], { timeout: 10 * 1000 }).then(() => {
			// we are ready to go
			webSocketServerClient.sendMessage({ op: SekitsuiOPCode.READY, d: null } as SekitsuiReady).catch(e => {
				// well fucc?
			});
		}).catch(e => {
			webSocketServerClient.close(SekitsuiCloseCode.IDENTIFY_TIMEOUT, `You took too long to identify.`);
		});

		// say hello
		const hello: SekitsuiHello = {
			op: SekitsuiOPCode.HELLO,
			d: {
				id: this.id,
				version: this.options.properties.version,
				heartbeatInterval: this.options.heartbeatInterval,
				properties: this.options.properties,
			},
		};

		try {
			await webSocketServerClient.sendMessage(hello);
		} catch (e) {
			// failed to send hello for some reason?
			// clean up i guess?
		}
	}

	private checkClientHeartbeats() {
		for (const client of this.clients.values()) {
			if (!client.lastHeartbeat) continue; // client does not seem ready yet

			const delta = Date.now() - client.lastHeartbeat;
			if (delta < 0) continue; // lol?

			// allow for heartbeatInterval + 50%
			// ie, 30 + (30 * .50) = 45, meaning the client has 45s
			// to send a hearbeat before being considered dead
			if (delta > this.options.heartbeatInterval + this.options.heartbeatInterval * 0.5) {
				client.close(SekitsuiCloseCode.SESSION_TIMEOUT, 'You did\'nt send enough heartbeats');
			}
		}

		// call again 2 sec later.
		// Done this way so we fully process client list before running again
		setTimeout(this.checkClientHeartbeats.bind(this), 2000);
	}
}

export interface WebSocketServer {
	on(event: WebSocketServerEvent.READY, fn: (port: number, host: string) => void): this;

	on(event: WebSocketServerEvent.CLIENT_CREATE, fn: (cli: WebSocketServerClient) => void): this;
	on(event: WebSocketServerEvent.CLIENT_DELETE, fn: (cli: WebSocketServerClient) => void): this;
	on(event: WebSocketServerEvent.CLIENT_READY, fn: (cli: WebSocketServerClient) => void): this;
	on(event: WebSocketServerEvent.CLIENT_MESSAGE, fn: (cli: WebSocketServerClient, message: SekitsuiMessage) => void): this;
	on(event: WebSocketServerEvent.CLIENT_CLOSE, fn: (cli: WebSocketServerClient, code: number, data?: string) => void): this;
	on(event: WebSocketServerEvent.CLIENT_ERROR, fn: (cli: WebSocketServerClient, e: Error) => void): this;
}
