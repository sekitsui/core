
import { SekitsuiOPCode } from '../../constants';
import { SekitsuiMessage } from './SekitsuiMessage';

export interface SekitsuiDispatch extends SekitsuiMessage {
	op: SekitsuiOPCode.DISPATCH;
	d: any;
	t?: never;
}
