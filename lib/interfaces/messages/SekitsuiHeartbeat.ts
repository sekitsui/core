
import { SekitsuiOPCode } from '../../constants';

export interface SekitsuiHeartbeat {
	op: SekitsuiOPCode.HEARTBEAT | SekitsuiOPCode.HEARTBEAT_ACK;
	d: any;
	t: never;
}
