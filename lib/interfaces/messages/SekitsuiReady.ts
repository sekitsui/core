
import { SekitsuiOPCode } from '../../constants';
import { SekitsuiMessage } from './SekitsuiMessage';

export interface SekitsuiReady extends SekitsuiMessage {
	op: SekitsuiOPCode.READY;
	d: {
		[key: string]: any;
	};
	t?: never;
}
