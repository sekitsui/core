'use strict';

import * as crypto from 'crypto';

export function createID(len: number = 24) {
	return crypto.randomBytes(len)
	.toString('base64')
	.replace(/[^a-z0-9]/gi, '')
	.slice(0, len);
}
