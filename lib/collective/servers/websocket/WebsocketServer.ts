'use strict';

import { IncomingMessage } from 'http';
import * as WebSocket from 'ws';

import { timedListener } from '@ayana/emitter-tools';

import { WebsocketServerClient } from './WebsocketServerClient';

import { SekitsuiCloseCode, SekitsuiOPCode } from '../../../constants';
import { SekitsuiHello, SekitsuiIdentify, SekitsuiMessage, SekitsuiReady } from '../../../interfaces/messages';

import { SekitsuiCollective } from '../../SekitsuiCollective';

export interface WebsocketServerProperties {
	version: string;
	[key: string]: any;
}

export interface WebsocketServerOptions {
	port: number;
	host?: string;
	heartbeatInterval?: number;
	behindProxy?: boolean;
	properties: WebsocketServerProperties;
	[key: string]: any;
}

export class WebsocketServer {
	public readonly id: string;
	public readonly type: string = 'WebsocketServer';
	public readonly collective: SekitsuiCollective;

	public readonly properties: WebsocketServerProperties;

	public readonly host: string;
	public readonly port: number;

	public readonly heartbeatInterval: number;
	private readonly behindProxy: boolean;

	private socket: WebSocket.Server;

	public readonly pendingClients: Map<string, WebsocketServerClient>;

	constructor(options: WebsocketServerOptions) {
		if (!options.port) throw new Error(`WebsocketServerOptions must define a port`);
		if (!options.properties) throw new Error(`WebsocketServerOptions must define properties`);

		// properties must define a version
		const properties = options.properties;
		if (!properties.version) throw new Error(`WebsocketServerProperties must define a version`);
		this.properties = properties;

		this.port = options.port;
		this.host = options.host || '127.0.0.1';

		this.heartbeatInterval = options.heartbeatInterval || 30 * 1000;
		this.behindProxy = options.behindProxy || false;

		this.pendingClients = new Map();
	}

	public async onLoad() {
		this.socket = new WebSocket.Server({
			host: this.host,
			port: this.port,
		});

		this.socket.on('connection', this.onConnection.bind(this));

		this.checkClientHeartbeats();
	}

	private async onConnection(socket: WebSocket, req: IncomingMessage) {
		let remoteAddress = req.connection.remoteAddress;
		// handle being behind a proxy
		if (this.behindProxy && req.headers['x-forwarded-for']) {
			remoteAddress = req.headers['x-forwarded-for'].toString();
		}

		// use left-most address (that's the client)
		remoteAddress = remoteAddress.split(',')[0].trim();
		const remotePort = req.connection.remotePort;
		const remoteFamily = req.connection.remoteFamily;

		const id = this.collective.createID();
		const websocketClient = new WebsocketServerClient(socket, id);
		websocketClient.ownerID = this.id;

		// set lastHeartbeat; prevents hearbeat loop from insta-killing this connection
		websocketClient.lastHeartbeat = Date.now();

		// prepare to handle pendingClient
		this.pendingClients.set(websocketClient.id, websocketClient);

		// prepare for identify
		const handleMessage = (identify: SekitsuiMessage): boolean => {
			if (identify.op !== SekitsuiOPCode.IDENTIFY) {
				// heartbeats are allowed right now
				if (identify.op === SekitsuiOPCode.HEARTBEAT || identify.op === SekitsuiOPCode.HEARTBEAT_ACK) return;

				// everything else is not
				websocketClient.close(SekitsuiCloseCode.NOT_IDENTIFIED, 'You sent a payload prior to identifying. Don\'t do this!');

				// do not continue
				return;
			}

			const result = this.handleClientIdentify(identify as SekitsuiIdentify);
			if (typeof result === 'string') {
				websocketClient.close(SekitsuiCloseCode.SESSION_TIMEOUT, result);
				return false;
			} else if (result === false) {
				websocketClient.close(SekitsuiCloseCode.UNKNOWN, 'Failed to identify');
				return false;
			}

			// client is no longer pending
			if (this.pendingClients.has(websocketClient.id)) this.pendingClients.delete(websocketClient.id);

			// add client to the collective
			this.collective.addClient(websocketClient);

			// handle client events
			websocketClient.on('message', (message: SekitsuiMessage) => {
				// handle heartbeats
				if (message.op === SekitsuiOPCode.HEARTBEAT) {
					websocketClient.lastHeartbeat = Date.now();

					websocketClient.sendMessage({
						op: SekitsuiOPCode.HEARTBEAT_ACK,
						d: null,
					});
				}

				this.collective.emit('clientMessage', message, websocketClient);
			});

			// handle close
			websocketClient.on('close', (code: number, message?: string) => {
				// remove from collective
				if (this.collective.hasClient(id)) this.collective.removeClient(id);
			});

			// tell everyone we ready
			this.collective.emit('clientReady', websocketClient);

			// inform the client of their ready status
			websocketClient.sendMessage({
				op: SekitsuiOPCode.READY,
				d: null,
			} as SekitsuiReady);

			// tell timedListener we got what we want
			return true;
		};

		// handle premature close
		const handleClose = (code: number, message?: string) => {
			// client is no longer pending
			if (this.pendingClients.has(id)) this.pendingClients.delete(id);

			// tell timedListener we got what we want
			return true;
		};

		// handle errors
		const handleError = (e: Error) => {
			// cleanup
			if (this.pendingClients.has(id)) this.pendingClients.delete(id);
			if (this.collective.hasClient(id)) this.collective.removeClient(id);

			// tell timedListener we got what we want
			return true;
		};

		// start listening
		timedListener(websocketClient, [
			{ eventName: 'message', fn: handleMessage },
			{ eventName: 'close', fn: handleClose },
			{ eventName: 'error', fn: handleError },
		], { timeout: 10 * 1000 })
		.catch((e: Error) => {
			if (this.pendingClients.has(id)) this.pendingClients.delete(id);
			websocketClient.close(SekitsuiCloseCode.IDENTIFY_TIMEOUT, `You took too long to identify.`);
		});

		// say hello
		const hello: SekitsuiHello = {
			op: SekitsuiOPCode.HELLO,
			d: {
				id: this.id,
				version: this.properties.version,
				heartbeatInterval: this.heartbeatInterval,
				properties: this.properties,
			},
		};

		try {
			await websocketClient.sendMessage(hello);
		} catch (e) {
			// failed to send hello for some reason?
			// clean up i guess?
			if (this.pendingClients.has(websocketClient.id)) this.pendingClients.delete(websocketClient.id);
			if (this.collective.hasClient(id)) this.collective.removeClient(id);
		}
	}

	public handleClientIdentify(identify: SekitsuiIdentify): boolean | string {
		return true;
	}

	private checkClientHeartbeats() {
		const clients = this.collective.getClients(this.id);

		for (const client of clients) {
			if (!client.lastHeartbeat) continue; // client does not seem ready yet

			const delta = Date.now() - client.lastHeartbeat;
			if (delta < 0) continue; // lol?

			if (delta > this.heartbeatInterval * 0.75) {
				client.close(SekitsuiCloseCode.SESSION_TIMEOUT, 'You did\'nt send enough heartbeats');
			}
		}

		// call again 2 sec later.
		// Done this way so we fully process client list before running again
		setTimeout(this.checkClientHeartbeats.bind(this), 2000);
	}
}
