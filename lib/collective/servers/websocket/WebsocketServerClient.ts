'use strict';

import { EventEmitter } from 'events';

import * as WebSocket from 'ws';

import { SekitsuiClient } from '../../../abstractions';
import { SekitsuiCloseCode } from '../../../constants';
import { SekitsuiMessage } from '../../../interfaces';

export class WebsocketServerClient extends SekitsuiClient {
	public readonly type: string = 'WebsocketServerClient';
	public id: string;

	private socket: WebSocket;

	constructor(socket: WebSocket, id: string) {
		super();
		this.id = id;
		this.socket = socket;

		this.socket.on('message', this.handleMessage.bind(this));
		this.socket.on('close', this.handleClose.bind(this));
		this.socket.on('error', this.handleError.bind(this));
	}

	private handleMessage(data: string) {
		let message: SekitsuiMessage;
		try {
			message = JSON.parse(data);
		} catch (e) {
			this.close(SekitsuiCloseCode.DECODE_ERROR, 'Failed to decode message');
			return;
		}

		this.emit('message', message);
	}

	private handleClose(code: number, data: string) {
		this.emit('close', code, data);
	}

	private handleError(error: Error) {
		this.emit('error', error);
	}

	public async sendMessage(message: SekitsuiMessage | string) {
		if (typeof message === 'object') {
			if (!message.op && message.op !== 0) throw new Error('Malformed Message: Must have valid OPCode.');
			if (!message.d && message.d !== null) throw new Error('Malformed Message: message data must be defined.');

			message = JSON.stringify(message);
		}

		if (this.socket.readyState !== WebSocket.OPEN) {
			throw new Error('WebSocket readyState does not equal open.');
		}

		return new Promise<void>((resolve, reject) => {
			this.socket.send(message, e => {
				if (e) return reject(e);
				resolve();
			});
		});
	}

	public close(code?: number, data?: string) {
		return this.socket.close(code, data);
	}
}
