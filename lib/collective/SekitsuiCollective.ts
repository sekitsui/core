'use strict';

import * as crypto from 'crypto';
import { EventEmitter } from 'events';

import { IllegalArgumentError, IllegalStateError } from '@ayana/errors';

import { SekitsuiMessage } from '../interfaces/messages';

import { SekitsuiClient } from '../abstractions';
import { SekitsuiOPCode } from '../constants';
import { SekitsuiServer } from '../interfaces';

export class SekitsuiCollective extends EventEmitter {
	private servers: Map<string, SekitsuiServer> = new Map();
	private clients: Map<string, SekitsuiClient> = new Map();

	public createID(len = 24) {
		return crypto.randomBytes(len)
		.toString('base64')
		.replace(/[^a-z0-9]/gi, '')
		.slice(0, len);
	}

	public async addServer(server: SekitsuiServer) {
		if (server == null || typeof server !== 'object') throw new IllegalArgumentError('Server must be a object');

		this.emit('serverCreate', server);
		return this.registerServer(server);
	}

	public async removeServer(id: string) {
		if (!id) throw new IllegalArgumentError('ID must be a string');
		if (!this.servers.has(id)) throw new IllegalStateError(`Server "${id}" does not exists`);

		const server = this.servers.get(id);
		this.emit('serverDelete', server);

		this.servers.delete(id);
	}

	private async registerServer(server: SekitsuiServer): Promise<string> {
		const id = this.createID();

		// attach id
		Object.defineProperty(server, 'id', {
			writable: false,
			configurable: false,
			enumerable: true,
			value: id,
		});

		// attach collective
		Object.defineProperty(server, 'collective', {
			writable: false,
			configurable: false,
			enumerable: true,
			value: this,
		});

		// call onLoad
		if (server.onLoad) {
			await server.onLoad();
		}

		this.emit('serverReady', server);

		this.servers.set(id, server);
		return id;
	}

	public hasClient(id: string) {
		if (!id) throw new IllegalArgumentError('ID must be a string');
		return this.clients.has(id);
	}

	public getClients(ownerID: string = null) {
		const clients = [];
		for (const [id, client] of this.clients.entries()) {
			if (ownerID) {
				if (client.ownerID === ownerID) clients.push(client);
			} else clients.push(client);
		}

		return clients;
	}

	public getClient(id: string) {
		if (!id) throw new IllegalArgumentError('ID must be a string');
		if (!this.clients.has(id)) return null;

		return this.clients.get(id);
	}

	public addClient(client: SekitsuiClient) {
		if (client == null || typeof client !== 'object') throw new IllegalArgumentError('Client must be a object');
		if (this.clients.has(client.id)) throw new IllegalStateError(`Client "${client.id}" is not unique`);

		this.clients.set(client.id, client);

		// event
		this.emit('clientCreate', client);
	}

	public removeClient(id: string) {
		if (!id) throw new IllegalArgumentError('ID must be a string');
		if (!this.clients.has(id)) throw new IllegalStateError(`Client "${id}" does not exists`);

		const client = this.clients.get(id);
		this.clients.delete(id);

		// event
		this.emit('clientDelete', client);
	}

	public async sendMessage(id: string, message: SekitsuiMessage) {
		if (!id) throw new IllegalArgumentError('ID must be a string');
		if (!this.clients.has(id)) throw new IllegalStateError(`Client "${id}" does not exist`);

		const client = this.clients.get(id);
		return client.sendMessage(message);
	}
}

export interface SekitsuiCollective {
	on(event: 'serverCreate', listener: (server: SekitsuiServer) => void): this;
	on(event: 'serverDelete', listener: (server: SekitsuiServer) => void): this;
	on(event: 'serverReady', listener: (server: SekitsuiServer) => void): this;

	on(event: 'clientCreate', listener: (client: SekitsuiClient) => void): this;
	on(event: 'clientDelete', listener: (client: SekitsuiClient) => void): this;
	on(event: 'clientReady', listener: (client: SekitsuiClient) => void): this;

	on(event: 'clientMessage', listener: (message: SekitsuiMessage, client: SekitsuiClient) => void): this;
}
