'use strict';

const { WebSocketServer } = require('../build');

const server = new WebSocketServer({
	host: '127.0.0.1',
	port: 27532,
	behindProxy: false,
	properties: {
		version: 'v0.0.1',
	},
});

server.on('ready', (port, host) => {
	console.log(`Ready! Serving on '${host}:${port}'`);
})

server.on('clientCreate', cli => {
	console.log(`New Client has just joined the server!`, cli.id);
});

server.on('clientReady', cli => {
	console.log(`Client successfully identified!`, cli.id);
});

server.on('clientDelete', (code, data, cli) => {
	console.log(`Client disconnected!`, code, data);
})

server.init();
