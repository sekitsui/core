'use strict';

const { SekitsuiCollective, WebsocketServer } = require('../build');
const collective = new SekitsuiCollective();

const websocket = new WebsocketServer({
	port: 27532,
	properties: {
		version: 'v1.0.0-alpha0',
	},
});

(async () => {
	collective.on('serverCreate', server => {
		console.log(`New server of type "${server.type}" created`);
	});

	collective.on('serverReady', server => {
		console.log(`${server.type} "${server.id}" is now ready!`);
		console.log(`Listening on ${server.host}:${server.port}`)
	});

	collective.on('clientMessage', (cli, message) => {
		console.log(`Got message from "${cli.id}"`);
		cli.sendMessage(message);
	});

	await collective.addServer(websocket);
})().catch(e => {
	console.error(e);
});
